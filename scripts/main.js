const selection = document.querySelectorAll(".icon");
let playerWins = 0;
let compWins = 0;

selection.forEach((selection) => {
  selection.addEventListener("click", function () {
    const playerInput = this.textContent;

    renderReset();

    document.getElementById("playIcon").src = `images/${playerInput}.ico`;

    const comSelection = ["Rock", "Paper", "Scissors", "Lizard", "Spock"];
    const comInput = comSelection[Math.floor(Math.random() * 5)];

    document.getElementById("comIcon").src = `images/${comInput}.ico`;

    compareInput(playerInput, comInput);
  });
});

function renderReset() {
  document.getElementById("playmove").classList.remove("win");
  document.getElementById("commove").classList.remove("win");
  document.getElementById("playmove").classList.remove("loss");
  document.getElementById("commove").classList.remove("loss");
}

function renderWin() {
  document.getElementById("playmove").classList.add("win");
  document.getElementById("commove").classList.remove("win");
  document.getElementById("playmove").classList.remove("loss");
  document.getElementById("commove").classList.add("loss");
}

function renderLoss() {
  document.getElementById("playmove").classList.remove("win");
  document.getElementById("commove").classList.add("win");
  document.getElementById("playmove").classList.add("loss");
  document.getElementById("commove").classList.remove("loss");
}

function compareInput(playerInput, comInput) {
  const currentMatch = `${playerInput} vs ${comInput}`;

  let result = document.getElementById("result");

  // if tie
  if (playerInput === comInput) {
    result.innerHTML = "It's a Tie!";
    renderDraw();
    return;
  }
  // Rock
  if (playerInput === "Rock") {
    if (comInput === "Scissors" || comInput === "Lizard") {
      result.innerHTML = "You Win!";
      playerWins++;
      document.getElementById("playerScore").innerHTML = playerWins;
      renderWin();
    } else {
      if (comInput === "Paper" || comInput === "Spock") {
        result.innerHTML = "You Lose!";
        compWins++;
        document.getElementById("computerScore").innerHTML = compWins;
        renderLoss();
      }
    }
  }
  // Paper
  if (playerInput === "Paper") {
    if (comInput === "Rock" || comInput === "Spock") {
      result.innerHTML = "You Win!";
      playerWins++;
      document.getElementById("playerScore").innerHTML = playerWins;
      renderWin();
    } else {
      if (comInput === "Scissors" || comInput === "Lizard") {
        result.innerHTML = "You Lose!";
        compWins++;
        document.getElementById("computerScore").innerHTML = compWins;
        renderLoss();
      }
    }
  }
  // Scissors
  if (playerInput === "Scissors") {
    if (comInput === "Paper" || comInput === "Lizard") {
      result.innerHTML = "You Win!";
      playerWins++;
      document.getElementById("playerScore").innerHTML = playerWins;
      renderWin();
    } else {
      if (comInput === "Rock" || comInput === "Spock") {
        result.innerHTML = "You Lose!";
        compWins++;
        document.getElementById("computerScore").innerHTML = compWins;
        renderLoss();
      }
    }
  }
  // Lizard
  if (playerInput === "Lizard") {
    if (comInput === "Paper" || comInput === "Spock") {
      result.innerHTML = "You Win!";
      playerWins++;
      document.getElementById("playerScore").innerHTML = playerWins;
      renderWin();
    } else {
      if (comInput === "Scissors" || comInput === "Rock") {
        result.innerHTML = "You Lose!";
        compWins++;
        document.getElementById("computerScore").innerHTML = compWins;
        renderLoss();
      }
    }
  }
  // Spock
  if (playerInput === "Spock") {
    if (comInput === "Scissors" || comInput === "Rock") {
      result.innerHTML = "You Win!";
      playerWins++;
      document.getElementById("playerScore").innerHTML = playerWins;
      renderWin();
    } else {
      if (comInput === "Paper" || comInput === "Lizard") {
        result.innerHTML = "You Lose!";
        compWins++;
        document.getElementById("computerScore").innerHTML = compWins;
        renderLoss();
      }
    }
  }
}
